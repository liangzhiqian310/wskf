Kefu
=================

Kefu基于GatewayWorker框架开发，完成客服系统数据通讯

[GatewayWorker手册](http://www.workerman.net/gatewaydoc/)

启动
=======
以debug方式启动

```php start.php start```

以daemon方式启动

```php start.php start -d```

停止

```php start.php stop```

平滑重启

```php start.php reload```

查看运行状态

```php start.php status```
