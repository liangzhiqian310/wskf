<?php
/**
 * Created by PhpStorm.
 * User: ChenYifan
 * Date: 2015/8/7
 * Time: 10:12
 */

namespace Config;

class MongoDB {

    /**
     * 获取实例方法
     * MongoDB::instance('kf')->kf->message->find();
     *
     * @var array
     */
    public static $kf = array(
        'server'  => 'mongodb://localhost:27017',
        'options' => array(
            'connect' => true,
        )
    );


}