<?php

if (ini_get('magic_quotes_gpc')) {
    function stripslashesRecursive(array $array) {
        foreach ($array as $k => $v) {
            if (is_string($v)) {
                $array[$k] = stripslashes($v);
            } else if (is_array($v)) {
                $array[$k] = stripslashesRecursive($v);
            }
        }

        return $array;
    }

    $_GET  = stripslashesRecursive($_GET);
    $_POST = stripslashesRecursive($_POST);
}

// 检测PHP环境
if (version_compare(PHP_VERSION, '5.3.0', '<')) die('require PHP > 5.3.0 !');

//网站当前路径

define('SITE_PATH', getcwd());

define('CLASS_PATH', './Class/');
define('COMMON_PATH', './Common/');
define('RUNTIME_PATH', './Runtime/');

// 开启调试模式 建议开发阶段开启 部署阶段注释或者设为false
define('APP_DEBUG', true);
// 定义应用目录
define('APP_PATH', './Application/');
define('BUILD_DIR_SECURE', true);
define('DIR_SECURE_CONTENT', 'deney Access!');
define('DIR_SECURE_FILENAME', 'index.html');

define('UPLOAD_PATH', './Upload/');

define('SITE_ROOT', '/');
define('SITE_DOMAIN', strip_tags($_SERVER['HTTP_HOST']));
define('SITE_URL', 'http://' . SITE_DOMAIN . SITE_ROOT);

define('THINK_PATH', './ThinkPHP323/');
define('WEB_PATH', './');
// 引入ThinkPHP入口文件
require THINK_PATH . 'ThinkPHP.php';

