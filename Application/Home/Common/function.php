<?php
/**
 * Created by PhpStorm.
 * User: ChenYifan
 * Date: 2015/8/5
 * Time: 18:02
 */

/**
 * 修改的 cache set 方法
 * @param     $key
 * @param     $value
 * @param int $expire
 */
function cache_set($key, $value, $expire = 0) {

    $config           = C('redis');
    $config['expire'] = $expire;

    S($key, $value, $config);
}

/**
 * 修改的 cache get 方法
 * @param $key
 * @return mixed
 */
function cache_get($key) {

    $cache  = S(C('redis'));
    $config = C('redis');

    return $cache->$key;
}

/**
 * 设置用户auth_token
 * @param $uid
 * @return string
 */
function setToken($uid) {

    $token = md5($uid . uniqid());
    $key   = 'kftoken_' . $uid;

    cache_set($key, $token);

    return $token;
}

/**
 * 获取用户auth_token
 * @param $uid
 * @return mixed
 */
function getToken($uid) {

    $key = 'kftoken_' . $uid;

    return cache_get($key);
}

/**
 * 验证 auth_token
 * @param $uid
 * @param $auth_token
 * @return bool
 */
function checkToken($uid, $token) {


    $key        = 'kftoken_' . $uid;
    $save_token = cache_get($key);

    if (empty($save_token) || $save_token != $token) {
        return false;
    }

    return true;
}

/**
 * 注销 清除auth_token
 * @param $uid
 */
function removeToken($uid) {

    $key = 'kftoken_' . $uid;
    cache_set($key, null);
}