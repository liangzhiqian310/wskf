<?php
/**
 * Created by PhpStorm.
 * User: ChenYifan
 * Date: 2015/8/5
 * Time: 17:17
 */

namespace Home\Controller;


class AuthController extends \Think\Controller {

    public function __construct() {
        parent::__construct(); // TODO: Change the autogenerated stub

        $user = session('user');
        if (!$user) {
            $this->redirect('Login/index');
        }
        
    }

} 