<?php
/**
 * Created by PhpStorm.
 * User: ChenYifan
 * Date: 2015/8/5
 * Time: 17:54
 */

namespace Home\Controller;


class LoginController extends \Think\Controller {


    public function index() {

        $this->display();
    }

    public function handle() {

        $map['account']  = I('post.account');
        $map['password'] = md5($_POST['password']);

        $user = M('kf')->where($map)->find();

        if ($user) {
            $user['token'] = setToken($user['id']);
            session('user', $user);
            $this->redirect('Index/index');
        } else {
            $this->error('账号或密码错误');
        }

    }

    public function logout(){

        session('user', null);
        $this->redirect('Login/index');
    }

} 