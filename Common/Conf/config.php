<?php

return array(

    'MODULE_DENY_LIST'     => array('Common', 'Runtime'),
    'MODULE_ALLOW_LIST'    => array('Home', 'Admin', 'Api', 'Doc'),
    'DEFAULT_MODULE'       => 'Home',

    'HTTP_CACHE_CONTROL'   => 'private',  // 网页缓存控制
    'CHECK_APP_DIR'        => true,       // 是否检查应用目录是否创建
    'FILE_UPLOAD_TYPE'     => 'Local',    // 文件上传方式
    'DATA_CRYPT_TYPE'      => 'Think',    // 数据加密方式

    'TMPL_PARSE_STRING'    => array(
        '__TMPL__'   => THEME_PATH,
        '__PUBLIC__' => '/Public/',
    ),

    'TOKEN_ON'             => true,  // 是否开启令牌验证
    'TOKEN_NAME'           => '__hash__',    // 令牌验证的表单隐藏字段名称
    'TOKEN_TYPE'           => 'md5',  //令牌哈希验证规则 默认为MD5
    'TOKEN_RESET'          => true,  //令牌验证出错后是否重置令牌 默认为true

    'URL_CASE_INSENSITIVE' => true,   // 默认false 表示URL区分大小写 true则表示不区分大小写
    'URL_MODEL'            => 2,       // URL访问模式,可选参数0、1、2、3,代表以下四种模式：

    /* 数据库设置 */
    'DB_TYPE'              => 'mysql',     // 数据库类型
    'DB_HOST'              => 'localhost', // 服务器地址
    'DB_NAME'              => 'kf',          // 数据库名
    'DB_USER'              => 'kf_user',      // 用户名
    'DB_PWD'               => 'kf23333',          // 密码

    'TMPL_FILE_DEPR'       => '_',

    'redis' => array(
        'type'   => 'redis',
        'host'   => '127.0.0.1',
        'port'   => '6379',
        'prefix' => 'kf_',
        'expire' => 0
    )
);